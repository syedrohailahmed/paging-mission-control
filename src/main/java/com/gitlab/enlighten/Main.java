package com.gitlab.enlighten;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;  
import com.google.gson.*;

public class Main {
	public static void main(String[] args) {
		
		try {
			String filename = args[0];
			
			if (filename==null) {
				System.out.println("No file name specified");
			}
			
			File file = new File(filename);
			FileReader fReader = new FileReader(file);
			BufferedReader bReader = new BufferedReader(fReader);
			String line; 
			List<TelemetryModelInput> listInput = new ArrayList<TelemetryModelInput>();
			List<TelemetryModelOutput> listOutput = new ArrayList<TelemetryModelOutput>();
			
			
			while((line = bReader.readLine())!=null) {
				String[] data = line.split("\\|");
				
				//<timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>

				TelemetryModelInput teleInput = new TelemetryModelInput();
				teleInput.setTimestamp(data[0]);
				teleInput.setSatellite_id(Integer.parseInt(data[1]));
				teleInput.setRed_high_limit(Float.parseFloat(data[2]));
				teleInput.setYellow_high_limit(Float.parseFloat(data[3]));
				teleInput.setYellow_low_limit(Float.parseFloat(data[4]));
				teleInput.setRed_low_limit(Float.parseFloat(data[5]));
				teleInput.setRaw_value(Float.parseFloat(data[6]));
				teleInput.setComponent(data[7]);
				
				listInput.add(teleInput);
				
				//System.out.println(teleInput.toString());
				
				//construct the output model
				TelemetryModelOutput teleOutput = new TelemetryModelOutput();
				teleOutput.setSatelliteId(teleInput.getSatellite_id());
				teleOutput.setSeverity(teleInput.getSeverity());
				teleOutput.setComponent(teleInput.getComponent());
				teleOutput.setTimestamp(teleInput.getTimestamp());
				
				listOutput.add(teleOutput);
				
			}
			
			//close readers
			bReader.close();
			fReader.close();
			
			if (listOutput.isEmpty()) {
				System.out.println("No output model was formed");
				System.exit(0);
			}
			
			String json = new Gson().toJson(listOutput);
			
			System.out.println("The JSON data is \n");
			System.out.println(json);
			
			Files.writeString(Paths.get("output.json"), json, StandardCharsets.UTF_8);
			
			System.out.println("\nThe JSON data is saved to output.json file. \nYou can verify the contents of the json file at http://jsonlint.com");
			
		} catch(Exception e) {
			System.out.println(e.toString());
		}
		
	}
}
