package com.gitlab.enlighten;


public class TelemetryModelInput {

	String timestamp;
	int satellite_id;
	float red_high_limit;
	float yellow_high_limit;
	float yellow_low_limit;
	float red_low_limit;
	float raw_value;
	String component;
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public int getSatellite_id() {
		return satellite_id;
	}
	public void setSatellite_id(int satellite_id) {
		this.satellite_id = satellite_id;
	}
	public float getRed_high_limit() {
		return red_high_limit;
	}
	public void setRed_high_limit(float red_high_limit) {
		this.red_high_limit = red_high_limit;
	}
	public float getYellow_high_limit() {
		return yellow_high_limit;
	}
	public void setYellow_high_limit(float yellow_high_limit) {
		this.yellow_high_limit = yellow_high_limit;
	}
	public float getYellow_low_limit() {
		return yellow_low_limit;
	}
	public void setYellow_low_limit(float yellow_low_limit) {
		this.yellow_low_limit = yellow_low_limit;
	}
	public float getRed_low_limit() {
		return red_low_limit;
	}
	public void setRed_low_limit(float red_low_limit) {
		this.red_low_limit = red_low_limit;
	}
	public float getRaw_value() {
		return raw_value;
	}
	public void setRaw_value(float raw_value) {
		this.raw_value = raw_value;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	
	public String getSeverity() {
		//<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>
		//1000			|101			 |98				 |25				|20				|102.9
		
		if (raw_value > red_high_limit) {
			return "RED HIGH";
		}
		
		if (raw_value < red_high_limit && raw_value > yellow_high_limit) {
			return "YELLOW HIGH";
		}
		
		if (raw_value < yellow_high_limit && raw_value > yellow_low_limit) {
			return "YELLOW LOW";
		}
		
		if (raw_value < yellow_low_limit && ((raw_value > red_low_limit) || (raw_value <= red_low_limit))) {
			return "RED LOW";
		}
		
		return "N/A";
		
	}
	
	public String toString() {
		return timestamp + "\n" + satellite_id + "\n" + red_high_limit + "\n" + yellow_high_limit + "\n" + yellow_low_limit + "\n" + red_low_limit + "\n" + raw_value + "\n" + component; 
	}
}
